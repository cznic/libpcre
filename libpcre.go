// Copyright 2023 The libpcre-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:generate go run generator.go

// Package libpcre is a ccgo/v4 version of libpcre.a (https://www.pcre.org)
package libpcre // import "modernc.org/libpcre"
